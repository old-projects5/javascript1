/**
 * Created by fkope on 19.2.2018.
 */

'use strict';

var progress = document.querySelector('.progress'),
    textarea = document.querySelector('textarea'),
    counter = document.querySelector('.counter');


// set Dasharray/offset
var pathLength = progress.getAttribute('r') * 2 * Math.PI,
    textLength = 30,
    warningZone = Math.floor(textLength * (1/2)),
    dangerZone = Math.floor(textLength * (3/4));

progress.style.strokeDasharray = pathLength + 'px';
progress.style.strokeDashoffset = pathLength + 'px';

// on input
textarea.addEventListener('input', function( event ){

    var len = textarea.value.length,
        per = len / textLength;
    console.log(len);

    // handle progress
    if ( len <= textLength ){
        let newOffset = pathLength - (pathLength * per) + 'px';
        progress.style.strokeDashoffset = newOffset;

        // handle colors
        progress.classList.toggle('warn', len > warningZone && len < dangerZone);
        progress.classList.toggle('danger', len >= dangerZone);
        progress.classList.toggle('tragedy', len == textLength);


    }


    // handle counter
    counter.textContent = textLength - len;
    counter.classList.toggle('danger', len >= textLength);
});

$('.textZone').highlightWithinTextarea({
    highlight: 'potato'
});